import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const RefreshTokenKey = 'Admin-Refresh-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function getRefreshToken() {
  return Cookies.get(RefreshTokenKey)
}

export function setToken(token, expire_seconds) {
  const expire_at = new Date(new Date().getTime() + expire_seconds * 1000)
  return Cookies.set(TokenKey, token, {
    expires: expire_at
  })
}

export function setRefreshToken(refreshToken, expire_seconds) {
  return Cookies.set(RefreshTokenKey, refreshToken)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function removeRefreshToken() {
  return Cookies.remove(RefreshTokenKey)
}
