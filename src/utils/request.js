import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import { getToken, getRefreshToken, removeToken, removeRefreshToken } from '@/utils/auth'
import router from '@/router'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api 的 base_url
  timeout: 5000, // request timeout
  headers: { 'X-Requested-With': 'XMLHttpRequest' }
})

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    if (store.getters.token) {
      // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
      config.headers['Authorization'] = `Bearer ${getToken()}`
    }
    return config
  },
  error => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    if (res.message) {
      Message({
        message: res.message,
        type: res.status,
        duration: 5 * 1000
      })
    }
    return response
  },

  /**
   * 下面的注释为通过在response里，自定义code来标示请求状态
   * 当code返回如下情况则说明权限有问题，登出并返回到登录页
   * 如想通过 xmlhttprequest 来状态码标识 逻辑可写在下面error中
   * 以下代码均为样例，请结合自生需求加以修改，若不需要，则可删除
   */
  // response => {
  //   const res = response.data
  //   if (res.code !== 20000) {
  //     Message({
  //       message: res.message,
  //       type: 'error',
  //       duration: 5 * 1000
  //     })
  //     // 50008:非法的token; 50012:其他客户端登录了;  50014:Token 过期了;
  //     if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
  //       // 请自行在引入 MessageBox
  //       // import { Message, MessageBox } from 'element-ui'
  //       MessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
  //         confirmButtonText: '重新登录',
  //         cancelButtonText: '取消',
  //         type: 'warning'
  //       }).then(() => {
  //         store.dispatch('FedLogOut').then(() => {
  //           location.reload() // 为了重新实例化vue-router对象 避免bug
  //         })
  //       })
  //     }
  //     return Promise.reject('error')
  //   } else {
  //     return response.data
  //   }
  // },
  error => {
    if (error.response.status !== 401) {
      // Handle Validation error
      if (error.response.status === 422) {
        const errors = error.response.data.errors
        const parag = document.createElement('p')
        for (const [field, messages] of Object.entries(errors)) {
          const ul = document.createElement('ul')
          ul.appendChild(document.createTextNode(field))
          messages.forEach((msg) => {
            const li = document.createElement('li')
            li.appendChild(document.createTextNode(msg))
            ul.appendChild(li)
          })
          parag.appendChild(ul)
        }
        Message({
          dangerouslyUseHTMLString: true,
          message: parag.innerHTML,
          type: 'error',
          duration: 5 * 1000
        })
      } else {
        if (error.response.data.message) {
          Message({
            message: error.response.data.message,
            type: error.response.data.status ? error.response.data.status : 'error',
            duration: 5 * 1000
          })
        }
      }
      return Promise.reject(error)
    }

    // Redirect if Refresh Token doesn't exists
    if (!getRefreshToken()) {
      return router.push('/login')
    }

    const originalRequest = error.config

    store.dispatch('RefreshToken', getRefreshToken())
      .then(response => {
        return service(originalRequest)
      }).catch(error => {
        console.log(error)
        removeRefreshToken()
        removeToken()
        router.push('/login')
      })
  }
)

export default service
