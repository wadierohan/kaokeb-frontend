import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    username,
    password
  }
  return request({
    url: '/api/login',
    method: 'post',
    data
  })
}

export function refreshToken(refresh_token) {
  const data = {
    refresh_token: refresh_token
  }
  return request({
    url: '/api/refresh_token',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: 'api/logout',
    method: 'post'
  })
}

export function getUserInfo() {
  return request({
    url: '/api/user',
    method: 'get'
  })
}

export function updateProfile(userInfo) {
  const data = {
    name: userInfo.name,
    email: userInfo.email,
    old_password: userInfo.old_password,
    password: userInfo.password,
    password_confirmation: userInfo.password_confirmation
  }
  return request({
    url: '/api/updateProfile',
    method: 'patch',
    data
  })
}

export function sendResetLink(email, callback) {
  const data = {
    email,
    callback
  }
  return request({
    url: '/api/sendResetLink',
    method: 'post',
    data
  })
}

export function resetPassword(payload) {
  const data = {
    email: payload.email,
    token: payload.token,
    password: payload.password,
    password_confirmation: payload.password_confirmation
  }
  return request({
    url: '/api/resetPassword',
    method: 'post',
    data
  })
}

