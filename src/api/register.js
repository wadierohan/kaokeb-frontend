import request from '@/utils/request'

export function registerNewUser(name, email, password, password_confirmation) {
  const data = {
    name,
    email,
    password,
    password_confirmation
  }

  return request({
    url: '/api/register',
    method: 'post',
    data
  })
}

