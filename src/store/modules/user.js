import { loginByUsername, logout, getUserInfo, updateProfile, sendResetLink, resetPassword } from '@/api/login'
import { registerNewUser } from '@/api/register'
import { getToken, setToken, removeToken, setRefreshToken, removeRefreshToken } from '@/utils/auth'
import { refreshToken } from '../../api/login'
import { Message } from 'element-ui'

const user = {
  state: {
    email: '',
    status: '',
    code: '',
    token: getToken(),
    name: '',
    avatar: '',
    introduction: '',
    roles: [],
    setting: {
      articlePlatform: []
    }
  },

  mutations: {
    SET_CODE: (state, code) => {
      state.code = code
    },
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_INTRODUCTION: (state, introduction) => {
      state.introduction = introduction
    },
    SET_SETTING: (state, setting) => {
      state.setting = setting
    },
    SET_STATUS: (state, status) => {
      state.status = status
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_EMAIL: (state, email) => {
      state.email = email
    }
  },

  actions: {
    // 用户名登录
    LoginByUsername({ commit }, userInfo) {
      const email = userInfo.email.trim()
      return new Promise((resolve, reject) => {
        loginByUsername(email, userInfo.password).then(response => {
          if (response && response.data) {
            const data = response.data
            commit('SET_TOKEN', data.access_token)
            setToken(data.access_token, data.expires_in)
            setRefreshToken(data.refresh_token)
          } else {
            // Wrong username or password
            Message({
              message: 'Email or password invalid',
              type: 'error',
              duration: 5 * 1000
            })
          }
          resolve()
        }).catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },

    // Refresh token
    RefreshToken({ commit }, refresh_token) {
      return new Promise((resolve, reject) => {
        refreshToken(refresh_token)
          .then((response) => {
            if (response) {
              const data = response.data
              commit('SET_TOKEN', data.access_token)
              setToken(data.access_token, data.expires_in)
              setRefreshToken(data.refresh_token)
              resolve()
            } else {
              reject('error')
            }
          }).catch(error => {
            reject(error)
          })
      })
    },

    // Register
    RegisterNewUser({ commit }, userInfo) {
      const email = userInfo.email.trim()
      return new Promise((resolve, reject) => {
        registerNewUser(userInfo.name, email, userInfo.password, userInfo.password_confirmation).then(response => {
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetUserInfo({ commit }) {
      return new Promise((resolve, reject) => {
        getUserInfo().then(response => {
          // 由于mockjs 不支持自定义状态码只能这样hack
          if (!response.data) {
            reject('Verification failed, please login again.')
          }
          const data = response.data

          if (data.roles && data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', data.roles)
          } else {
            reject('getInfo: roles must be a non-null array!')
          }

          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          commit('SET_EMAIL', data.email)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 第三方验证登录
    // LoginByThirdparty({ commit, state }, code) {
    //   return new Promise((resolve, reject) => {
    //     commit('SET_CODE', code)
    //     loginByThirdparty(state.status, state.email, state.code).then(response => {
    //       commit('SET_TOKEN', response.data.token)
    //       setToken(response.data.token)
    //       resolve()
    //     }).catch(error => {
    //       reject(error)
    //     })
    //   })
    // },

    // 登出
    LogOut({ commit }) {
      return new Promise((resolve, reject) => {
        logout().then((response) => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          removeRefreshToken()
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        removeRefreshToken()
        resolve()
      })
    },

    // 动态修改权限
    ChangeRoles({ commit, dispatch }, role) {
      return new Promise(resolve => {
        getUserInfo(role).then(response => {
          const data = response.data
          commit('SET_ROLES', data.roles)
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          commit('SET_INTRODUCTION', data.introduction)
          dispatch('GenerateRoutes', data) // 动态修改权限后 重绘侧边菜单
          resolve()
        })
      })
    },

    // Update Profile
    UpdateProfile({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        updateProfile(userInfo).then(response => {
          const data = response.data.data
          commit('SET_NAME', data.name)
          commit('SET_EMAIL', data.email)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // Sent Reset Link
    SendResetLink({ commit }, { email }) {
      return new Promise((resolve, reject) => {
        sendResetLink(email, window.location.href).then(response => {
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // Reset Password
    ResetPassword({ commit }, payload) {
      return new Promise((resolve, reject) => {
        resetPassword(payload).then(response => {
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default user
