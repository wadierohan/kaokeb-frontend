# Kaokeb Test

Ceci est la suite de la première partie : https://bitbucket.org/wadierohan/kaokeb-backend

### Installation

Cloner le repository, puis installer les dépendances puis lancer les commandes de configuration :

```sh
$ git clone https://bitbucket.org/wadierohan/kaokeb-frontend.git
$ cd kaokeb-frontend
$ npm install
```
Il faut configurer la communication avec le serveur backend dans les fichiers de config :
  - config/dev.env.js #Pour le développement
  - config/sit.env.js #Pour le testing
  - config/prod.env.js #Pour la production
```js
    VUE_APP_BASE_API: '"http://kaokeb-backend.test"'
```
Il faut changer la valeur suivante :
 - VUE_APP_BASE_API: La base du serveur backend.

### Running

Une fois la configuration faite, il ne reste plus qu'à lancer le serveur :
```sh
# Environement DEV
npm dev #Ceci lancera automatiquement la page : http://localhost:9527

# Environement Test
npm run build:stage

# Environement Production
npm run build:prod
```

**Un grand merci à l'équipe [Kaokeb](https://kaokeb.com) pour cet intéressant test**
**Wadie ELARRIM**